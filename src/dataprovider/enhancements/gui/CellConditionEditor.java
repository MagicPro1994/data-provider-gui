package dataprovider.enhancements.gui;

import javax.swing.DefaultCellEditor;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import dataprovider.antlr4.exparser.ConditionSyntaxChecker;
import dataprovider.antlr4.exparser.ConditionSyntaxError;

import java.awt.Component;
import java.util.List;

public class CellConditionEditor extends DefaultCellEditor {
	private static final long serialVersionUID = 6451871612925543309L;
	JTextField conditionText = new JTextField();

	public CellConditionEditor() {
		super(new JTextField());
	}

	@Override
	public boolean stopCellEditing() {
		String cellValue = getCellEditorValue().toString();
		List<ConditionSyntaxError> errorList = ConditionSyntaxChecker.getSyntaxErrors(cellValue);
		if (errorList.size() > 0) {
			String errorMessages = new String("");
			for(ConditionSyntaxError e: errorList) {
				errorMessages += String.format("Error at %d: %s\n", e.getCharPositionInLine(), e.getMessage());
			}
			JOptionPane.showMessageDialog(null, errorMessages, "Condition Syntax Error", JOptionPane.WARNING_MESSAGE);
			return false;
		}
		fireEditingStopped();
		return true;
	}

	@Override
	public Object getCellEditorValue() {
		return this.conditionText.getText();
	}

	@Override
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {

		Object fieldValue = value;
		if (null == fieldValue)
			fieldValue = "";

		this.conditionText.setEditable(true);
		this.conditionText.setText(fieldValue.toString());
		return this.conditionText;
	}

}

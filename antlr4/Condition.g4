grammar Condition;

parse_method: method_expression? EOF;
parse_condition: (method_expression | expression)? EOF;

method_expression: methodCall ('.' ambiguousName)?;
methodCall: methodName LPAREN methodCallArguments RPAREN;
methodCallArguments: // No arguments
	| expression (',' expression)*; // Some arguments

expression:
	LPAREN expression RPAREN								# parenExpression
	| NOT expression										# notExpression
	| left = expression op = comparator right = expression	# comparatorExpression
	| left = expression op = binary right = expression		# binaryExpression
	| bool													# boolExpression
	| ambiguousName											# identifierExpression
	| specialName											# specialExpression
	| DECIMAL												# decimalExpression
	| StringLiteral											# stringExpression;

methodName: Identifier;
specialName: '#.' ambiguousName;
ambiguousName: Identifier | ambiguousName '.' Identifier;

comparator: GT | GE | LT | LE | EQ | NE;
binary: AND | OR;
bool: TRUE | FALSE;

AND: 'AND' | 'and';
OR: 'OR' | 'or';
NOT: 'NOT' | 'not';
TRUE: 'TRUE' | 'true';
FALSE: 'FALSE' | 'false';
GT: '>';
GE: '>=';
LT: '<';
LE: '<=';
EQ: '=';
NE: '<>';
LPAREN: '(';
RPAREN: ')';
DECIMAL: '-'? [0-9]+ ( '.' [0-9]+)?;
StringLiteral: '"' ( ~[\\"] | '\\' [\\"])* '"';
//IDENTIFIER: ID_PREFIX? [a-zA-Z_] [a-zA-Z0-9]* | STRING; METHOD_IDENTIFIER: [$]? NAME;

Identifier: JavaLetter JavaLetterOrDigit*;

fragment JavaLetter:
	[a-zA-Z$_] // these are the "java letters" below 0x7F
	| // covers all characters above 0x7F which are not a surrogate
	~[\u0000-\u007F\uD800-\uDBFF] {Character.isJavaIdentifierStart(_input.LA(-1))}?
	| // covers UTF-16 surrogate pairs encodings for U+10000 to U+10FFFF
	[\uD800-\uDBFF] [\uDC00-\uDFFF] {Character.isJavaIdentifierStart(Character.toCodePoint((char)_input.LA(-2), (char)_input.LA(-1)))
		}?;

fragment JavaLetterOrDigit:
	[a-zA-Z0-9$_] // these are the "java letters or digits" below 0x7F
	| // covers all characters above 0x7F which are not a surrogate
	~[\u0000-\u007F\uD800-\uDBFF] {Character.isJavaIdentifierPart(_input.LA(-1))}?
	| // covers UTF-16 surrogate pairs encodings for U+10000 to U+10FFFF
	[\uD800-\uDBFF] [\uDC00-\uDFFF] {Character.isJavaIdentifierPart(Character.toCodePoint((char)_input.LA(-2), (char)_input.LA(-1)))
		}?;

// Skip space characters
WS: [ \r\t\u000C\n]+ -> skip;

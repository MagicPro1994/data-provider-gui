// Generated from Condition.g4 by ANTLR 4.7.2
package antlr4.exparser;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class ConditionLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.7.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, AND=4, OR=5, NOT=6, TRUE=7, FALSE=8, GT=9, GE=10, 
		LT=11, LE=12, EQ=13, NE=14, LPAREN=15, RPAREN=16, DECIMAL=17, StringLiteral=18, 
		Identifier=19, WS=20;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	private static String[] makeRuleNames() {
		return new String[] {
			"T__0", "T__1", "T__2", "AND", "OR", "NOT", "TRUE", "FALSE", "GT", "GE", 
			"LT", "LE", "EQ", "NE", "LPAREN", "RPAREN", "DECIMAL", "StringLiteral", 
			"Identifier", "JavaLetter", "JavaLetterOrDigit", "WS"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'.'", "','", "'#.'", null, null, null, null, null, "'>'", "'>='", 
			"'<'", "'<='", "'='", "'<>'", "'('", "')'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, "AND", "OR", "NOT", "TRUE", "FALSE", "GT", "GE", 
			"LT", "LE", "EQ", "NE", "LPAREN", "RPAREN", "DECIMAL", "StringLiteral", 
			"Identifier", "WS"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public ConditionLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "Condition.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	@Override
	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 19:
			return JavaLetter_sempred((RuleContext)_localctx, predIndex);
		case 20:
			return JavaLetterOrDigit_sempred((RuleContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean JavaLetter_sempred(RuleContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return Character.isJavaIdentifierStart(_input.LA(-1));
		case 1:
			return Character.isJavaIdentifierStart(Character.toCodePoint((char)_input.LA(-2), (char)_input.LA(-1)))
				;
		}
		return true;
	}
	private boolean JavaLetterOrDigit_sempred(RuleContext _localctx, int predIndex) {
		switch (predIndex) {
		case 2:
			return Character.isJavaIdentifierPart(_input.LA(-1));
		case 3:
			return Character.isJavaIdentifierPart(Character.toCodePoint((char)_input.LA(-2), (char)_input.LA(-1)))
				;
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\26\u00ae\b\1\4\2"+
		"\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"+
		"\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22"+
		"\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\3\2\3\2\3\3\3"+
		"\3\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\5\5=\n\5\3\6\3\6\3\6\3\6\5\6C\n"+
		"\6\3\7\3\7\3\7\3\7\3\7\3\7\5\7K\n\7\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\5"+
		"\bU\n\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\5\ta\n\t\3\n\3\n\3\13"+
		"\3\13\3\13\3\f\3\f\3\r\3\r\3\r\3\16\3\16\3\17\3\17\3\17\3\20\3\20\3\21"+
		"\3\21\3\22\5\22w\n\22\3\22\6\22z\n\22\r\22\16\22{\3\22\3\22\6\22\u0080"+
		"\n\22\r\22\16\22\u0081\5\22\u0084\n\22\3\23\3\23\3\23\3\23\7\23\u008a"+
		"\n\23\f\23\16\23\u008d\13\23\3\23\3\23\3\24\3\24\7\24\u0093\n\24\f\24"+
		"\16\24\u0096\13\24\3\25\3\25\3\25\3\25\3\25\3\25\5\25\u009e\n\25\3\26"+
		"\3\26\3\26\3\26\3\26\3\26\5\26\u00a6\n\26\3\27\6\27\u00a9\n\27\r\27\16"+
		"\27\u00aa\3\27\3\27\2\2\30\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25"+
		"\f\27\r\31\16\33\17\35\20\37\21!\22#\23%\24\'\25)\2+\2-\26\3\2\n\3\2\62"+
		";\4\2$$^^\6\2&&C\\aac|\4\2\2\u0081\ud802\udc01\3\2\ud802\udc01\3\2\udc02"+
		"\ue001\7\2&&\62;C\\aac|\5\2\13\f\16\17\"\"\2\u00bc\2\3\3\2\2\2\2\5\3\2"+
		"\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21"+
		"\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2"+
		"\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3"+
		"\2\2\2\2-\3\2\2\2\3/\3\2\2\2\5\61\3\2\2\2\7\63\3\2\2\2\t<\3\2\2\2\13B"+
		"\3\2\2\2\rJ\3\2\2\2\17T\3\2\2\2\21`\3\2\2\2\23b\3\2\2\2\25d\3\2\2\2\27"+
		"g\3\2\2\2\31i\3\2\2\2\33l\3\2\2\2\35n\3\2\2\2\37q\3\2\2\2!s\3\2\2\2#v"+
		"\3\2\2\2%\u0085\3\2\2\2\'\u0090\3\2\2\2)\u009d\3\2\2\2+\u00a5\3\2\2\2"+
		"-\u00a8\3\2\2\2/\60\7\60\2\2\60\4\3\2\2\2\61\62\7.\2\2\62\6\3\2\2\2\63"+
		"\64\7%\2\2\64\65\7\60\2\2\65\b\3\2\2\2\66\67\7C\2\2\678\7P\2\28=\7F\2"+
		"\29:\7c\2\2:;\7p\2\2;=\7f\2\2<\66\3\2\2\2<9\3\2\2\2=\n\3\2\2\2>?\7Q\2"+
		"\2?C\7T\2\2@A\7q\2\2AC\7t\2\2B>\3\2\2\2B@\3\2\2\2C\f\3\2\2\2DE\7P\2\2"+
		"EF\7Q\2\2FK\7V\2\2GH\7p\2\2HI\7q\2\2IK\7v\2\2JD\3\2\2\2JG\3\2\2\2K\16"+
		"\3\2\2\2LM\7V\2\2MN\7T\2\2NO\7W\2\2OU\7G\2\2PQ\7v\2\2QR\7t\2\2RS\7w\2"+
		"\2SU\7g\2\2TL\3\2\2\2TP\3\2\2\2U\20\3\2\2\2VW\7H\2\2WX\7C\2\2XY\7N\2\2"+
		"YZ\7U\2\2Za\7G\2\2[\\\7h\2\2\\]\7c\2\2]^\7n\2\2^_\7u\2\2_a\7g\2\2`V\3"+
		"\2\2\2`[\3\2\2\2a\22\3\2\2\2bc\7@\2\2c\24\3\2\2\2de\7@\2\2ef\7?\2\2f\26"+
		"\3\2\2\2gh\7>\2\2h\30\3\2\2\2ij\7>\2\2jk\7?\2\2k\32\3\2\2\2lm\7?\2\2m"+
		"\34\3\2\2\2no\7>\2\2op\7@\2\2p\36\3\2\2\2qr\7*\2\2r \3\2\2\2st\7+\2\2"+
		"t\"\3\2\2\2uw\7/\2\2vu\3\2\2\2vw\3\2\2\2wy\3\2\2\2xz\t\2\2\2yx\3\2\2\2"+
		"z{\3\2\2\2{y\3\2\2\2{|\3\2\2\2|\u0083\3\2\2\2}\177\7\60\2\2~\u0080\t\2"+
		"\2\2\177~\3\2\2\2\u0080\u0081\3\2\2\2\u0081\177\3\2\2\2\u0081\u0082\3"+
		"\2\2\2\u0082\u0084\3\2\2\2\u0083}\3\2\2\2\u0083\u0084\3\2\2\2\u0084$\3"+
		"\2\2\2\u0085\u008b\7$\2\2\u0086\u008a\n\3\2\2\u0087\u0088\7^\2\2\u0088"+
		"\u008a\t\3\2\2\u0089\u0086\3\2\2\2\u0089\u0087\3\2\2\2\u008a\u008d\3\2"+
		"\2\2\u008b\u0089\3\2\2\2\u008b\u008c\3\2\2\2\u008c\u008e\3\2\2\2\u008d"+
		"\u008b\3\2\2\2\u008e\u008f\7$\2\2\u008f&\3\2\2\2\u0090\u0094\5)\25\2\u0091"+
		"\u0093\5+\26\2\u0092\u0091\3\2\2\2\u0093\u0096\3\2\2\2\u0094\u0092\3\2"+
		"\2\2\u0094\u0095\3\2\2\2\u0095(\3\2\2\2\u0096\u0094\3\2\2\2\u0097\u009e"+
		"\t\4\2\2\u0098\u0099\n\5\2\2\u0099\u009e\6\25\2\2\u009a\u009b\t\6\2\2"+
		"\u009b\u009c\t\7\2\2\u009c\u009e\6\25\3\2\u009d\u0097\3\2\2\2\u009d\u0098"+
		"\3\2\2\2\u009d\u009a\3\2\2\2\u009e*\3\2\2\2\u009f\u00a6\t\b\2\2\u00a0"+
		"\u00a1\n\5\2\2\u00a1\u00a6\6\26\4\2\u00a2\u00a3\t\6\2\2\u00a3\u00a4\t"+
		"\7\2\2\u00a4\u00a6\6\26\5\2\u00a5\u009f\3\2\2\2\u00a5\u00a0\3\2\2\2\u00a5"+
		"\u00a2\3\2\2\2\u00a6,\3\2\2\2\u00a7\u00a9\t\t\2\2\u00a8\u00a7\3\2\2\2"+
		"\u00a9\u00aa\3\2\2\2\u00aa\u00a8\3\2\2\2\u00aa\u00ab\3\2\2\2\u00ab\u00ac"+
		"\3\2\2\2\u00ac\u00ad\b\27\2\2\u00ad.\3\2\2\2\22\2<BJT`v{\u0081\u0083\u0089"+
		"\u008b\u0094\u009d\u00a5\u00aa\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}
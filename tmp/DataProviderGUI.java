import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.JLayeredPane;
import javax.swing.JTabbedPane;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JMenu;
import javax.swing.SwingConstants;

import java.awt.event.KeyEvent;
import javax.swing.KeyStroke;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.Dimension;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class DataProviderGUI {

	private JFrame frmDataProviderGenerator;
	private JTextField txtApiGroup;
	private JTextField txtApiVersion;
	private JTextField txtApiName;
	private JTextField txtApiUri;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DataProviderGUI window = new DataProviderGUI();
					window.frmDataProviderGenerator.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public DataProviderGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmDataProviderGenerator = new JFrame();
		frmDataProviderGenerator.setTitle("Data Provider Generator");
		frmDataProviderGenerator.setBounds(100, 100, 640, 480);
		frmDataProviderGenerator.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmDataProviderGenerator.getContentPane().setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setAlignmentY(0.0f);
		tabbedPane.setAlignmentX(0.0f);
		tabbedPane.setBorder(null);
		tabbedPane.setBounds(10, 45, 604, 358);
		frmDataProviderGenerator.getContentPane().add(tabbedPane);
		
		JLayeredPane layeredPane = new JLayeredPane();
		tabbedPane.addTab("Positive", null, layeredPane, null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBorder(new EmptyBorder(2, 0, 0, 0));
		scrollPane.setAutoscrolls(true);
		scrollPane.setBounds(0, 45, 600, 285);
		layeredPane.add(scrollPane);
		
		JButton btnAddRow = new JButton("Add Row");
		btnAddRow.setBounds(10, 11, 89, 23);
		layeredPane.add(btnAddRow);
		
		JButton btnAddColumn_1 = new JButton("Add Column");
		btnAddColumn_1.setBounds(109, 11, 89, 23);
		layeredPane.add(btnAddColumn_1);
		
		JButton button = new JButton("Add Column");
		button.setBounds(208, 11, 89, 23);
		layeredPane.add(button);
		
		JLayeredPane layeredPane_1 = new JLayeredPane();
		tabbedPane.addTab("New tab", null, layeredPane_1, null);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBorder(new EmptyBorder(2, 0, 0, 0));
		scrollPane_1.setAutoscrolls(true);
		scrollPane_1.setBounds(0, 45, 600, 285);
		layeredPane_1.add(scrollPane_1);
		
		JButton button_1 = new JButton("Add Row");
		button_1.setBounds(10, 11, 89, 23);
		layeredPane_1.add(button_1);
		
		JButton button_2 = new JButton("Add Column");
		button_2.setBounds(109, 11, 89, 23);
		layeredPane_1.add(button_2);
		
		JButton button_3 = new JButton("Add Column");
		button_3.setBounds(208, 11, 89, 23);
		layeredPane_1.add(button_3);
		
		JLabel lblApiGroup = new JLabel("Group:");
		lblApiGroup.setBounds(10, 14, 33, 14);
		frmDataProviderGenerator.getContentPane().add(lblApiGroup);
		
		lblApiGroup.setLabelFor(txtApiGroup);
		txtApiGroup = new JTextField();
		txtApiGroup.setToolTipText("The group of API");
		
		txtApiGroup.setText("Customer");
		txtApiGroup.setBounds(44, 11, 100, 20);
		frmDataProviderGenerator.getContentPane().add(txtApiGroup);
		txtApiGroup.setColumns(10);
		
		txtApiVersion = new JTextField();
		txtApiVersion.setToolTipText("The version of API");
		txtApiVersion.setText("1.0");
		txtApiVersion.setColumns(10);
		txtApiVersion.setBounds(195, 11, 50, 20);
		frmDataProviderGenerator.getContentPane().add(txtApiVersion);
		
		JLabel lblApiVersion = new JLabel("Version:");
		lblApiVersion.setLabelFor(txtApiVersion);
		lblApiVersion.setBounds(154, 14, 40, 14);
		frmDataProviderGenerator.getContentPane().add(lblApiVersion);
		
		JLabel lblApiName = new JLabel("Name:");
		lblApiName.setBounds(255, 14, 33, 14);
		frmDataProviderGenerator.getContentPane().add(lblApiName);
		
		txtApiName = new JTextField();
		txtApiName.setToolTipText("The name of API");
		txtApiName.setText("GetCustomerByID");
		txtApiName.setColumns(10);
		txtApiName.setBounds(289, 11, 100, 20);
		frmDataProviderGenerator.getContentPane().add(txtApiName);
		
		JLabel lblApiUri = new JLabel("URI:");
		lblApiUri.setBounds(399, 14, 22, 14);
		frmDataProviderGenerator.getContentPane().add(lblApiUri);
		
		txtApiUri = new JTextField();
		txtApiUri.setToolTipText("The URI of API");
		txtApiUri.setText("http://test.com/Customer/");
		txtApiUri.setColumns(10);
		txtApiUri.setBounds(422, 11, 191, 20);
		frmDataProviderGenerator.getContentPane().add(txtApiUri);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBorderPainted(false);
		frmDataProviderGenerator.setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu("File");
		mnFile.setPreferredSize(new Dimension(30, 25));
		mnFile.setMnemonic('f');
		menuBar.add(mnFile);
		
		JMenuItem mntmLoad = new JMenuItem("Load");
		mntmLoad.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		    	JOptionPane.showMessageDialog(frmDataProviderGenerator, "Eggs are not supposed to be green.");
			}
		});
				
		mntmLoad.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L, InputEvent.CTRL_MASK));
		mntmLoad.setMnemonic('l');
		mntmLoad.setHorizontalTextPosition(SwingConstants.LEFT);
		mntmLoad.setHorizontalAlignment(SwingConstants.LEFT);
		mnFile.add(mntmLoad);
		
		JMenuItem mntmGenerate = new JMenuItem("Generate");
		mntmGenerate.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G, InputEvent.CTRL_MASK));
		mntmGenerate.setMnemonic('g');
		mnFile.add(mntmGenerate);
		
		JMenu mnAction = new JMenu("Action");
		mnAction.setMnemonic('a');
		menuBar.add(mnAction);
		
		JMenu mnPanel = new JMenu("Panel");
		mnPanel.setMnemonic('p');
		mnAction.add(mnPanel);
		
		JMenuItem mntmNewPanel = new JMenuItem("New");
		mntmNewPanel.setOpaque(true);
		mntmNewPanel.setMnemonic('n');
		mntmNewPanel.setHorizontalTextPosition(SwingConstants.LEFT);
		mntmNewPanel.setHorizontalAlignment(SwingConstants.LEFT);
		mnPanel.add(mntmNewPanel);
		
		JMenuItem mntmRenamePanel = new JMenuItem("Rename");
		mntmRenamePanel.setMnemonic('r');
		mntmRenamePanel.setToolTipText("Rename active tab panel");
		mnPanel.add(mntmRenamePanel);
		
		JMenuItem mntmDeletePanel = new JMenuItem("Delete");
		mntmDeletePanel.setToolTipText("Delete active tab panel");
		mntmDeletePanel.setMnemonic('d');
		mnPanel.add(mntmDeletePanel);
		
		JMenu mnTable = new JMenu("Table");
		mnTable.setMnemonic('t');
		mnAction.add(mnTable);
		
		JMenuItem mntmNewColumn = new JMenuItem("New");
		mnTable.add(mntmNewColumn);
		mntmNewColumn.setOpaque(true);
		mntmNewColumn.setMnemonic('t');
		mntmNewColumn.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, InputEvent.CTRL_MASK));
		mntmNewColumn.setHorizontalTextPosition(SwingConstants.LEFT);
		mntmNewColumn.setHorizontalAlignment(SwingConstants.LEFT);
		
		JMenuItem mntmRename = new JMenuItem("Rename");
		mnTable.add(mntmRename);
		
		JMenuItem mntmDeleteCurrentTab = new JMenuItem("Delete tab");
		mnTable.add(mntmDeleteCurrentTab);
		mntmDeleteCurrentTab.setMnemonic('d');
		mntmDeleteCurrentTab.setToolTipText("Delete active tab");
		
		JMenu mnHelp = new JMenu("Help");
		mnHelp.setMnemonic('h');
		menuBar.add(mnHelp);
	}
}

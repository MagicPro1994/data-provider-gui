/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dataprovider.enhancements.gui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;

import javax.swing.plaf.TableHeaderUI;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import dataprovider.enhancements.interfaces.IColumnGroup;

@SuppressWarnings("serial")
public class GroupableTableHeader extends JTableHeader {
	private IColumnGroup draggedGroup;

	public GroupableTableHeader(TableColumnModel model) {
		super(model);
		super.setUI(new GroupableTableHeaderUI());
		setReorderingAllowed(true);
	}

	@Override
	public void setUI(TableHeaderUI ui) {
	}

	public void setDraggedGroup(IColumnGroup columnGroup) {
		draggedGroup = columnGroup;
	}

	public IColumnGroup getDraggedGroup() {
		return draggedGroup;
	}

	// ================================================================================
	// Support methods
	// ================================================================================
	/**
	 * Get groupable header bounds for column
	 * 
	 * @param column TableColumn
	 * @return Rectangle
	 */
	public Rectangle getBoundsFor(TableColumn column) {
		Rectangle bounds = new Rectangle();
		TableColumnModel columnModel = this.getColumnModel();
		if (columnModel instanceof GroupableColumnModel) {
			GroupableColumnModel groupModel = (GroupableColumnModel) columnModel;
			IColumnGroup group = groupModel.getColumnGroupFor(column);
			bounds = getBoundsFor(group);
		}
		return bounds;
	}

	/**
	 * Get groupable header bounds for group column
	 * 
	 * @param group IColumnGroup
	 * @return Rectangle
	 */
	public Rectangle getBoundsFor(IColumnGroup group) {
		Rectangle bounds = new Rectangle();
		TableColumnModel columnModel = this.getColumnModel();
		if (columnModel instanceof GroupableColumnModel) {
			// GroupableColumnModel groupModel = (GroupableColumnModel) columnModel;
			if (group != null) {
				TableColumn firstColumn = group.getColumnAt(0);
				Dimension size = getGroupSize(group);
				bounds = new Rectangle(size);
				bounds.y = 0;
				int lastColumnIndex = columnModel.getColumnIndex(firstColumn.getIdentifier());
				for (int index = 0; index < lastColumnIndex; index++) {
					TableColumn tc = columnModel.getColumn(index);
					bounds.x += tc.getWidth();
				}
			}
		}
		return bounds;
	}

	/**
	 * Get header group size
	 * 
	 * @param group IColumnGroup
	 * @return Dimension
	 */
	protected Dimension getGroupSize(IColumnGroup group) {
		Dimension size = new Dimension();
		TableColumnModel columnModel = this.getColumnModel();
		if (columnModel instanceof GroupableColumnModel) {
			GroupableColumnModel groupModel = (GroupableColumnModel) columnModel;
			TableCellRenderer renderer = getRenderer(group);
			Component comp = renderer.getTableCellRendererComponent(this.getTable(), group.getTitle(), false, false, -1,
					-1);
			size.height += comp.getPreferredSize().height;
			for (TableColumn column : groupModel.getGroupColumns(group)) {
				size.width += column.getWidth();
			}
		}
		return size;
	}

	/**
	 * Get table cell renderer for column group
	 * 
	 * @param group IColumnGroup
	 * @return TableCellRenderer
	 */
	protected TableCellRenderer getRenderer(IColumnGroup group) {
		TableCellRenderer renderer = group.getRenderer();
		if (renderer == null) {
			renderer = this.getDefaultRenderer();
		}
		return renderer;
	}
	
	public boolean canResizeColumn(Point p, TableColumn column) {
		Rectangle bounds = this.getBoundsFor(column);
		return (bounds != null && !bounds.contains(p)) && (column != null) && this.getResizingAllowed()
				&& column.getResizable();
	}
	
	public TableColumn getResizingColumn(Point p, int column) {
		if (column == -1) {
			return null;
		}

		Rectangle r = this.getHeaderRect(column);
		r.grow(-3, 0);
		if (r.contains(p)) {
			return null;
		}

		int midPoint = r.x + r.width / 2;
		int columnIndex;
		if (this.getComponentOrientation().isLeftToRight()) {
			columnIndex = (p.x < midPoint) ? column - 1 : column;
		} else {
			columnIndex = (p.x < midPoint) ? column : column - 1;
		}

		if (columnIndex == -1) {
			return null;
		}

		return this.getColumnModel().getColumn(columnIndex);

	}
}

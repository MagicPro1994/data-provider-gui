// Generated from Condition.g4 by ANTLR 4.7.2
package antlr4.exparser;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link ConditionParser}.
 */
public interface ConditionListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link ConditionParser#parse_condition}.
	 * @param ctx the parse tree
	 */
	void enterParse_condition(ConditionParser.Parse_conditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link ConditionParser#parse_condition}.
	 * @param ctx the parse tree
	 */
	void exitParse_condition(ConditionParser.Parse_conditionContext ctx);
	/**
	 * Enter a parse tree produced by {@link ConditionParser#method_expression}.
	 * @param ctx the parse tree
	 */
	void enterMethod_expression(ConditionParser.Method_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link ConditionParser#method_expression}.
	 * @param ctx the parse tree
	 */
	void exitMethod_expression(ConditionParser.Method_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link ConditionParser#methodCall}.
	 * @param ctx the parse tree
	 */
	void enterMethodCall(ConditionParser.MethodCallContext ctx);
	/**
	 * Exit a parse tree produced by {@link ConditionParser#methodCall}.
	 * @param ctx the parse tree
	 */
	void exitMethodCall(ConditionParser.MethodCallContext ctx);
	/**
	 * Enter a parse tree produced by {@link ConditionParser#methodCallArguments}.
	 * @param ctx the parse tree
	 */
	void enterMethodCallArguments(ConditionParser.MethodCallArgumentsContext ctx);
	/**
	 * Exit a parse tree produced by {@link ConditionParser#methodCallArguments}.
	 * @param ctx the parse tree
	 */
	void exitMethodCallArguments(ConditionParser.MethodCallArgumentsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code binaryExpression}
	 * labeled alternative in {@link ConditionParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterBinaryExpression(ConditionParser.BinaryExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code binaryExpression}
	 * labeled alternative in {@link ConditionParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitBinaryExpression(ConditionParser.BinaryExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code decimalExpression}
	 * labeled alternative in {@link ConditionParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterDecimalExpression(ConditionParser.DecimalExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code decimalExpression}
	 * labeled alternative in {@link ConditionParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitDecimalExpression(ConditionParser.DecimalExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code stringExpression}
	 * labeled alternative in {@link ConditionParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterStringExpression(ConditionParser.StringExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code stringExpression}
	 * labeled alternative in {@link ConditionParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitStringExpression(ConditionParser.StringExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code boolExpression}
	 * labeled alternative in {@link ConditionParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterBoolExpression(ConditionParser.BoolExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code boolExpression}
	 * labeled alternative in {@link ConditionParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitBoolExpression(ConditionParser.BoolExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code identifierExpression}
	 * labeled alternative in {@link ConditionParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterIdentifierExpression(ConditionParser.IdentifierExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code identifierExpression}
	 * labeled alternative in {@link ConditionParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitIdentifierExpression(ConditionParser.IdentifierExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code notExpression}
	 * labeled alternative in {@link ConditionParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterNotExpression(ConditionParser.NotExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code notExpression}
	 * labeled alternative in {@link ConditionParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitNotExpression(ConditionParser.NotExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code specialExpression}
	 * labeled alternative in {@link ConditionParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterSpecialExpression(ConditionParser.SpecialExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code specialExpression}
	 * labeled alternative in {@link ConditionParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitSpecialExpression(ConditionParser.SpecialExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code parenExpression}
	 * labeled alternative in {@link ConditionParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterParenExpression(ConditionParser.ParenExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code parenExpression}
	 * labeled alternative in {@link ConditionParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitParenExpression(ConditionParser.ParenExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code comparatorExpression}
	 * labeled alternative in {@link ConditionParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterComparatorExpression(ConditionParser.ComparatorExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code comparatorExpression}
	 * labeled alternative in {@link ConditionParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitComparatorExpression(ConditionParser.ComparatorExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link ConditionParser#methodName}.
	 * @param ctx the parse tree
	 */
	void enterMethodName(ConditionParser.MethodNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link ConditionParser#methodName}.
	 * @param ctx the parse tree
	 */
	void exitMethodName(ConditionParser.MethodNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link ConditionParser#specialName}.
	 * @param ctx the parse tree
	 */
	void enterSpecialName(ConditionParser.SpecialNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link ConditionParser#specialName}.
	 * @param ctx the parse tree
	 */
	void exitSpecialName(ConditionParser.SpecialNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link ConditionParser#ambiguousName}.
	 * @param ctx the parse tree
	 */
	void enterAmbiguousName(ConditionParser.AmbiguousNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link ConditionParser#ambiguousName}.
	 * @param ctx the parse tree
	 */
	void exitAmbiguousName(ConditionParser.AmbiguousNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link ConditionParser#comparator}.
	 * @param ctx the parse tree
	 */
	void enterComparator(ConditionParser.ComparatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link ConditionParser#comparator}.
	 * @param ctx the parse tree
	 */
	void exitComparator(ConditionParser.ComparatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link ConditionParser#binary}.
	 * @param ctx the parse tree
	 */
	void enterBinary(ConditionParser.BinaryContext ctx);
	/**
	 * Exit a parse tree produced by {@link ConditionParser#binary}.
	 * @param ctx the parse tree
	 */
	void exitBinary(ConditionParser.BinaryContext ctx);
	/**
	 * Enter a parse tree produced by {@link ConditionParser#bool}.
	 * @param ctx the parse tree
	 */
	void enterBool(ConditionParser.BoolContext ctx);
	/**
	 * Exit a parse tree produced by {@link ConditionParser#bool}.
	 * @param ctx the parse tree
	 */
	void exitBool(ConditionParser.BoolContext ctx);
}
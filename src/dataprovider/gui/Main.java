package dataprovider.gui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.API;
import com.Info;
import com.github.underscore.lodash.U;

import dataprovider.common.Constants;
import dataprovider.common.enums.LoadType;
import dataprovider.enhancements.Helpers;
import dataprovider.enhancements.JTableHelpers;

public class Main {
	// ================================================================================
	// Variables
	// ================================================================================
	private JFrame frmMain;
	private JTabbedPane tabbedPane;
	private JTextField txtApiGroup;
	private JTextField txtApiVersion;
	private JTextField txtApiName;
	private JTextField txtApiUri;

	// ================================================================================
	// Initialize GUI and launch it
	// ================================================================================
	/**
	 * Initialize the application.
	 */
	public Main() {
		Info.Working_Space = Constants.CURRENT_DIR;
		initialize();
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main window = new Main();
					window.frmMain.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		setMainFrameProperty();
		addMenuToolBar();
		addAPIDataFields();
		addContainerTabPane();
		loadAppData(LoadType.DEFAULT_TEMPLATE_FILE);
	}

	/**
	 * Set initial main frame's properties
	 */
	private void setMainFrameProperty() {
		frmMain = new JFrame();
		frmMain.setTitle("Data Provider Generator");
		frmMain.setBounds(100, 100, 800, 600);
		frmMain.setResizable(false);
		frmMain.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMain.getContentPane().setLayout(null);
	}

	/**
	 * Add menu tool bars
	 */
	private void addMenuToolBar() {
		JMenuBar menuBar = new JMenuBar();

		// File >
		JMenu mnFile = new JMenu("File");
		mnFile.setPreferredSize(new Dimension(30, 25));
		mnFile.setMnemonic('f');
		menuBar.add(mnFile);

		// File > Load
		JMenuItem mntmLoad = new JMenuItem("Load");
		mntmLoad.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L, InputEvent.CTRL_MASK));
		mntmLoad.setMnemonic('l');
		mntmLoad.setToolTipText("Load API data from XML file");
		mntmLoad.addActionListener(e -> loadAppData(LoadType.FROM_XML));
		mnFile.add(mntmLoad);

		// File > Generate
		JMenuItem mntmGenerate = new JMenuItem("Generate Dataprovider Files");//Export to XML
		mntmGenerate.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G, InputEvent.CTRL_MASK));
		mntmGenerate.setMnemonic('e');
		mntmGenerate.setToolTipText("Generate API data (json file, test-case file, xml file"); //Export to application data to XML file
		mntmGenerate.addActionListener(e -> exportXML());
		mnFile.add(mntmGenerate);

		// Action
		JMenu mnAction = new JMenu("Action");
		mnAction.setMnemonic('a');
		menuBar.add(mnAction);

		JMenu mnPanel = new JMenu("Panel");
		mnPanel.setMnemonic('p');
		mnPanel.setToolTipText("Actions related to create/modify panel (Case)");
		mnAction.add(mnPanel);

		JMenuItem mntmNewPanel = new JMenuItem("New");
		mntmNewPanel.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_MASK));
		mntmNewPanel.setMnemonic('n');
		mntmNewPanel.setToolTipText("Add new panel (Case)");
		mntmNewPanel.addActionListener(e -> createPanel());
		mnPanel.add(mntmNewPanel);

		JMenuItem mntmRenamePanel = new JMenuItem("Rename");
		mntmRenamePanel.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F2, 0));
		mntmRenamePanel.setMnemonic('r');
		mntmRenamePanel.setToolTipText("Rename active tab panel");
		mntmRenamePanel.addActionListener(e -> renamePanel());
		mnPanel.add(mntmRenamePanel);

		JMenuItem mntmDeletePanel = new JMenuItem("Delete");
		mntmDeletePanel.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D, InputEvent.CTRL_MASK));
		mntmDeletePanel.setMnemonic('d');
		mntmDeletePanel.setToolTipText("Delete active tab panel");
		mntmDeletePanel.addActionListener(e -> deletePanel());
		mnPanel.add(mntmDeletePanel);


		frmMain.setJMenuBar(menuBar);
	}

	/**
	 * Add API Data fields: Group, Version, Name, URI
	 */
	private void addAPIDataFields() {
		// API Group
		JLabel lblApiGroup = new JLabel("Group:");
		lblApiGroup.setLabelFor(txtApiGroup);
		lblApiGroup.setBounds(10, 14, 50, 14);

		txtApiGroup = new JTextField();
		txtApiGroup.setToolTipText("The group of API");
		txtApiGroup.setBounds(51, 11, 100, 20);

		// API Version
		JLabel lblApiVersion = new JLabel("Version:");
		lblApiVersion.setLabelFor(txtApiVersion);
		lblApiVersion.setBounds(161, 14, 50, 14);

		txtApiVersion = new JTextField();
		txtApiVersion.setToolTipText("The version of API");
		txtApiVersion.setBounds(212, 11, 50, 20);

		// API Name
		JLabel lblApiName = new JLabel("Name:");
		lblApiName.setLabelFor(txtApiName);
		lblApiName.setBounds(272, 14, 50, 14);

		txtApiName = new JTextField();
		txtApiName.setToolTipText("The name of API");
		txtApiName.setBounds(313, 11, 150, 20);

		// API URI
		JLabel lblApiUri = new JLabel("URI:");
		lblApiUri.setLabelFor(txtApiUri);
		lblApiUri.setBounds(473, 14, 27, 14);

		txtApiUri = new JTextField();
		txtApiUri.setToolTipText("The URI of API");
		txtApiUri.setBounds(501, 11, 200, 20);

		frmMain.getContentPane().add(lblApiGroup);
		frmMain.getContentPane().add(lblApiVersion);
		frmMain.getContentPane().add(lblApiName);
		frmMain.getContentPane().add(lblApiUri);

		frmMain.getContentPane().add(txtApiGroup);
		frmMain.getContentPane().add(txtApiVersion);
		frmMain.getContentPane().add(txtApiName);
		frmMain.getContentPane().add(txtApiUri);
	}

	/**
	 * Add tab container
	 */
	private void addContainerTabPane() {
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setAlignmentY(0.0f);
		tabbedPane.setAlignmentX(0.0f);
		tabbedPane.setBorder(null);
		tabbedPane.setBounds(10, 45, 774, 488);
		frmMain.getContentPane().add(tabbedPane);
	}

	// ================================================================================
	// Event functions
	// ================================================================================
	/**
	 * Load application data
	 * 
	 * @param loadType LoadType
	 */
	private void loadAppData(LoadType loadType) {
		switch (loadType) {
		case DEFAULT_TEMPLATE_FILE:
			// Load Application Data from file
			loadAppDataFrom(Helpers.loadDataFromFile(Constants.DEFAULT_TEMPLATE_FILE));
			break;
		case FROM_XML:
			// Load Application Data from file dialog
			loadAppDataFrom(new FileNameExtensionFilter("XML File", "xml"));
			break;
		case FROM_JSON:
			// Load Application Data from file dialog
			loadAppDataFrom(new FileNameExtensionFilter("JSON File", "json"));
			break;
		default:
			// Load Application Data from file
			loadAppDataFrom(Helpers.loadDataFromFile(Constants.DEFAULT_TEMPLATE_FILE));
			break;
		}
	}

	/**
	 * Load Application Data from file dialog
	 * 
	 * @param filter FileNameExtensionFilter
	 */
	private void loadAppDataFrom(FileNameExtensionFilter fileFilter) {
		final JFileChooser jfc = new JFileChooser(Constants.CURRENT_DIR);
		jfc.setAcceptAllFileFilterUsed(false);
		jfc.setFileFilter(fileFilter);
		if (jfc.showDialog(frmMain, "Load") == JFileChooser.APPROVE_OPTION) {
			try {
				loadAppDataFrom(Helpers.loadDataFromFile(jfc.getSelectedFile().getPath()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Load Application Data from map parsed from XML/JSON file
	 * 
	 * @param objData LinkedHashMap<?, ?>
	 */
	private void loadAppDataFrom(LinkedHashMap<?, ?> objData) {
		try {
			tabbedPane.removeAll();
			loadAPIData(objData);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Load API Data to application
	 * 
	 * @param objData LinkedHashMap<?, ?>
	 */
	private void loadAPIData(LinkedHashMap<?, ?> objData) {
		Object apiData = objData.get("API");
		if (apiData instanceof LinkedHashMap) {
			loadAPIInfo((LinkedHashMap<?, ?>) apiData);
			loadAPICases((LinkedHashMap<?, ?>) apiData);
		}
	}

	/**
	 * Load API basic information: Group, Name, Version, URI
	 * 
	 * @param apiData
	 */
	private void loadAPIInfo(LinkedHashMap<?, ?> apiData) {
		try {
			txtApiGroup.setText((String) apiData.get("-group"));
			txtApiName.setText((String) apiData.get("-name"));
			txtApiVersion.setText((String) apiData.get("-version"));
			txtApiUri.setText((String) apiData.get("-uri"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String generateXMLTextFromAppData() {
		LinkedHashMap<String, Object> xmlMap = new LinkedHashMap<>();
		List<LinkedHashMap<String, Object>> caseList = new ArrayList<>();
		LinkedHashMap<String, Object> apiDataMap = new LinkedHashMap<>();
		for (int i = 0; i < tabbedPane.getComponentCount(); i++) {
			LinkedHashMap<String, Object> apiCaseMap = new LinkedHashMap<>();
			String caseTitle = tabbedPane.getTitleAt(i);
			JTable table = getTableFromTab((JLayeredPane) tabbedPane.getComponentAt(i));
			List<LinkedHashMap<String, Object>> argumentList = JTableHelpers.convertTableToAPIData(table);
			apiCaseMap.put("-title", caseTitle);
			if (argumentList.size() > 1) {
				apiCaseMap.put("Argument", argumentList);
			} else {
				apiCaseMap.put("Argument", argumentList.get(0));
			}
			caseList.add(apiCaseMap);
		}
		if (caseList.size() > 1) {
			apiDataMap.put("Case", caseList);
		} else if (caseList.size() == 1) {
			apiDataMap.put("Case", caseList.get(0));
		}
		apiDataMap.put("-name", txtApiName.getText());
		apiDataMap.put("-group", txtApiGroup.getText());
		apiDataMap.put("-version", txtApiVersion.getText());
		apiDataMap.put("-uri", txtApiUri.getText());
		xmlMap.put("API", apiDataMap);
		return U.toXml(xmlMap);
	}

	private void exportXML() {
		final JFileChooser jfc = new JFileChooser(Constants.CURRENT_DIR);
		jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		if (jfc.showDialog(frmMain, "Export") == JFileChooser.APPROVE_OPTION) {
			try {
				String xmlContent = generateXMLTextFromAppData();
				String filePath = String.format("%s\\%s\\%s\\%s.xml", jfc.getSelectedFile().getPath(), txtApiVersion.getText(), txtApiGroup.getText(), txtApiName.getText());
				File file = new File(filePath);
				file.getParentFile().mkdirs();
				Helpers.writeTextToFile(filePath, xmlContent);
				API oAPI = new API(filePath);
	            oAPI.Generate();
	            oAPI.WriteTestCase();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Create new template tab panel
	 */
	private void createPanel() {
		String paneName = JOptionPane.showInputDialog(frmMain, "Please input new name: ");
		if (paneName != null) {
			createCaseTab(paneName);
		}
	}

	/**
	 * Rename active tab panel
	 */
	private void renamePanel() {
		int selectedIndex = tabbedPane.getSelectedIndex();
		String oldName = tabbedPane.getTitleAt(selectedIndex);
		String paneName = JOptionPane.showInputDialog(frmMain, "Please enter new name: ", oldName);
		if (paneName != null && !oldName.equals(paneName)) {
			tabbedPane.setTitleAt(selectedIndex, paneName);
		}
	}

	/**
	 * Delete active tab panel
	 */
	private void deletePanel() {
		if (JOptionPane.showConfirmDialog(frmMain, "Do you want to delete active tab?") == JOptionPane.OK_OPTION) {
			int selectedIndex = tabbedPane.getSelectedIndex();
			tabbedPane.remove(selectedIndex);
		}
	}

	// ================================================================================
	// Utility functions
	// ================================================================================
	/**
	 * Load API cases (tabs in application)
	 * 
	 * @param apiData
	 */
	private void loadAPICases(LinkedHashMap<?, ?> apiData) {
		try {
			// Get API Cases data
			Object apiCases = apiData.get("Case");
			// If apiCase is LinkedHashMap, it means that API object has only one case
			if (apiCases instanceof LinkedHashMap) {
				createCaseTab((LinkedHashMap<?, ?>) apiCases);
				return;
			}
			// If apiCase is ArrayList, it means that API object has at least two cases
			if (apiCases instanceof ArrayList) {
				for (Object el : ((ArrayList<?>) apiCases)) {
					createCaseTab((LinkedHashMap<?, ?>) el);
				}
				return;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Generate default table container
	 * 
	 * @return JScrollPane
	 */
	private JScrollPane generateTableContainer() {
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setAutoscrolls(true);
		scrollPane.setBounds(0, 0, 772, 462);
		return scrollPane;
	}

	private JTable getTableFromTab(JLayeredPane layeredPane) {
		Component tableContainer = layeredPane.getComponent(0);
		if (tableContainer instanceof JScrollPane) {
			return (JTable) ((JScrollPane) tableContainer).getViewport().getView();
		}
		return null;
	}
	

	/**
	 * Create default case tab
	 * 
	 * @param paneTitle
	 */
	private void createCaseTab(String paneTitle) {
		JLayeredPane layeredPane = new JLayeredPane();
		JTable table = JTableHelpers.createTable();
		JScrollPane scrollPane = generateTableContainer();
		scrollPane.setViewportView(table);
		layeredPane.add(scrollPane);
		tabbedPane.addTab(paneTitle, null, layeredPane, "A case of API data");
	}

	/**
	 * Create case tab from API Case Data
	 * 
	 * @param apiCaseData LinkedHashMap
	 */
	private void createCaseTab(LinkedHashMap<?, ?> apiCaseData) {
		try {
			String paneTitle = (String) apiCaseData.get("-title");
			// Create layers' container
			JLayeredPane layeredPane = new JLayeredPane();
			// Create table
			JTable table = JTableHelpers.createTable(apiCaseData.get("Argument"));
			// Create table's container
			JScrollPane scrollPane = generateTableContainer();
			scrollPane.setViewportView(table);
			layeredPane.add(scrollPane);
			tabbedPane.addTab(paneTitle, null, layeredPane, "A case of API data");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
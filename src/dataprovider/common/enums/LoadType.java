package dataprovider.common.enums;

public enum LoadType {
	DEFAULT_TEMPLATE_FILE,
	FROM_DIALOG,
	FROM_XML,
	FROM_JSON
}
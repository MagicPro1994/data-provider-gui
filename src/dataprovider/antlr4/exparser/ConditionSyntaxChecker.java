package dataprovider.antlr4.exparser;

import org.antlr.v4.runtime.CodePointCharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import antlr4.exparser.ConditionLexer;
import antlr4.exparser.ConditionParser;

import java.util.List;

import static org.antlr.v4.runtime.CharStreams.fromString;

public class ConditionSyntaxChecker {

	public static List<ConditionSyntaxError> getSyntaxErrors(String sourceCode) {
		CodePointCharStream inputStream = fromString(sourceCode);
		ConditionLexer lexer = new ConditionLexer(inputStream);
		CommonTokenStream commonTokenStream = new CommonTokenStream(lexer);
		ConditionParser parser = new ConditionParser(commonTokenStream);
		ConditionSyntaxErrorListener listener = new ConditionSyntaxErrorListener();
		parser.addErrorListener(listener);
		// parser.parse();
		ParseTree tree = parser.parse_condition();
		System.out.println(tree.toStringTree(parser));
		return listener.getSyntaxErrors();
	}
}
package dataprovider.antlr4.exparser;

import java.util.ArrayList;
import java.util.List;

public class Exparser {

    public static void main(String[] args) {
    	List<String> expressions = new ArrayList<String>();
    	expressions.add("");
    	expressions.add("( A > -3.4)");
    	expressions.add("(( A > C) AND NOT(C <D)) AND (DB.ID = \"12345>AKKD\")");
    	expressions.add("((A = \"xyz\") AND(C=\"USER.ID>=4\") AND FALSE)");
    	expressions.add("((A = \"xyz\") and (C=\"USER.ID>=4\") or true)");
    	expressions.add("a AND (AD > \"a>.3\")");
    	expressions.add("DB.User");
    	expressions.add("(A>B)AND(C<D)");
    	expressions.add("(A=\"xyz>abc\") AND (USER.ID<>\"abc\")");
    	expressions.add("RandomD(DB.ID, \"DAD\")");
    	expressions.add("$GetObjs(#.Abc, DB.ID>\"2\" AND DB.Name<\"5\").ID");
    	expressions.add("$Get_Objs(-3, DB.ID>\"2\" AND DB.Name<\"5\").ID");
    	for(String exp:expressions) {
    		System.out.println("---- Checking syntax: " + exp);
    		List<ConditionSyntaxError> errorList = ConditionSyntaxChecker.getSyntaxErrors(exp);
    		if(errorList.isEmpty())
    			System.out.println("PASSED");
    		else {
    			for(ConditionSyntaxError e: errorList) {
    				System.out.println(e.getMessage());
    			}
    		}
    	}
    }
}
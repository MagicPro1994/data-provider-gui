package dataprovider.enhancements.interfaces;

import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

public interface IColumnGroup {
	public String getTitle();
	public TableCellRenderer getRenderer();
	public void addColumn(TableColumn column);
	public void removeColumn(TableColumn column);
	public TableColumn getColumnAt(int column);
	public int getColumnCount();
	public boolean contains(TableColumn column);
}

package dataprovider.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import com.github.underscore.lodash.U;

import dataprovider.enhancements.Helpers;
import dataprovider.enhancements.gui.GroupableColumnModel;
import dataprovider.enhancements.gui.GroupableTableHeader;
import dataprovider.enhancements.interfaces.IColumnGroup;

public class TestGUI {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TestGUI window = new TestGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public TestGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		JTable table = new JTable();
		table.setAutoCreateColumnsFromModel(false);
		table.setShowGrid(true);
		table.setGridColor(Color.GRAY);

		DefaultTableModel tm = new DefaultTableModel(0, 10);

		for (int index = 0; index < 10; index++) {
			Object[] row = new Object[10];
			for (int col = 0; col < 10; col++) {
				row[col] = index + "x" + col;
			}
			tm.addRow(row);
		}
		table.setModel(tm);

		GroupableColumnModel model = new GroupableColumnModel();
		model.addColumn(createColumn("Name", 0));
		model.addColumn(createColumn("Role", 1));
		model.addColumn(createColumn("Type", 2));
		model.addColumn(createColumn("Number", 3));
		model.addColumn(createColumn("Condition", 4));
		model.addColumn(createColumn("1", 5));
		model.addColumn(createColumn("2", 6));
		model.addColumn(createColumn("3", 7));
		model.addColumn(createColumn("4", 8));
		model.addColumn(createColumn("5", 9));

		IColumnGroup groupA = model.addGroup("Arguments (+)");
		groupA.addColumn(model.getColumn(model.getColumnIndex("Name")));
		groupA.addColumn(model.getColumn(model.getColumnIndex("Role")));
		groupA.addColumn(model.getColumn(model.getColumnIndex("Type")));
		groupA.addColumn(model.getColumn(model.getColumnIndex("Number")));
		groupA.addColumn(model.getColumn(model.getColumnIndex("Condition")));

		IColumnGroup groupB = model.addGroup("Data (+)");
		groupB.addColumn(model.getColumn(model.getColumnIndex("1")));
		groupB.addColumn(model.getColumn(model.getColumnIndex("2")));
		groupB.addColumn(model.getColumn(model.getColumnIndex("3")));
		groupB.addColumn(model.getColumn(model.getColumnIndex("4")));
		groupB.addColumn(model.getColumn(model.getColumnIndex("5")));

		table.setColumnModel(model);
		table.setTableHeader(new GroupableTableHeader(table.getColumnModel()));

		frame = new JFrame("Testing");
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout());
		frame.getContentPane().add(new JScrollPane(table));
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);

		List<LinkedHashMap<String, Object>> argumentList = new ArrayList<>();

		LinkedHashMap<String, Object> caseMap = new LinkedHashMap<>();
		GroupableColumnModel gm = (GroupableColumnModel) table.getColumnModel();
		Vector<?> data = ((DefaultTableModel) table.getModel()).getDataVector();
		List<IColumnGroup> gList = gm.getGroups();
		for (int i = 0; i < data.size(); i++) {
			@SuppressWarnings("unchecked")
			Vector<Object> row = (Vector<Object>) data.elementAt(i);
			LinkedHashMap<String, Object> argumentMap = new LinkedHashMap<>();
			List<LinkedHashMap<String, Object>> dataList = new ArrayList<>();
			for (IColumnGroup group : gList) {
				String groupTitle = group.getTitle();
				List<TableColumn> colList = gm.getGroupColumns(group);
				if (groupTitle.contains("Argument")) {
					for (TableColumn col : colList) {
						Object colTitle = col.getHeaderValue();
						argumentMap.put(Helpers.fromTitleToAttribute((String) colTitle),
								row.elementAt(gm.getColumnIndex(colTitle)));
					}
				}
				if (groupTitle.contains("Data")) {
					for (TableColumn col : colList) {
						LinkedHashMap<String, Object> dataMap = new LinkedHashMap<>();
						Object colTitle = col.getHeaderValue();
						dataMap.put("-idx", colTitle);
						dataMap.put("#text", row.elementAt(gm.getColumnIndex(colTitle)));
						dataList.add(dataMap);
					}
				}
			}
			argumentMap.put("Data", dataList);
			argumentList.add(argumentMap);
		}
		caseMap.put("Argument", argumentList);
		System.out.println(U.toXml(caseMap));
	}

	protected static TableColumn createColumn(String title, int modelIndex) {
		TableColumn column = new TableColumn();
		column.setHeaderValue(title);
		column.setIdentifier(title);
		column.setModelIndex(modelIndex);
		return column;
	}

}

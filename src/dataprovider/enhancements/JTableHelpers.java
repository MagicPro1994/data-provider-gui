package dataprovider.enhancements;

import java.awt.Color;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Vector;
import java.util.Map.Entry;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import dataprovider.common.Constants;
import dataprovider.enhancements.gui.CellComboBoxEditor;
import dataprovider.enhancements.gui.CellComboBoxRenderer;
import dataprovider.enhancements.gui.CellConditionEditor;
import dataprovider.enhancements.gui.GroupableColumnModel;
import dataprovider.enhancements.gui.GroupableTableHeader;
import dataprovider.enhancements.interfaces.ClickedListener;
import dataprovider.enhancements.interfaces.IColumnGroup;

public class JTableHelpers {
	/**
	 * Generate default JTable
	 * 
	 * @return JTable
	 */
	public static JTable createTemplateTable() {
		JTable table = new JTable();
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		table.setAutoCreateColumnsFromModel(false);
		table.setRowHeight(22);
		table.setShowGrid(true);
		table.setGridColor(Color.GRAY);
		return table;
	}

	/**
	 * Create default table
	 * 
	 * @return JTable
	 */
	public static JTable createTable() {
		JTable table = createTemplateTable();
		GroupableColumnModel model = createTableHeaders();
		table.setColumnModel(model);
		DefaultTableModel tm = new DefaultTableModel(1, model.getColumnCount());
		table.setModel(tm);
		table.setTableHeader(new GroupableTableHeader(table.getColumnModel()));
		JTableHeader tblHeader = table.getTableHeader();
		// Extend group header mouse event
		tblHeader.addMouseListener((ClickedListener) (e) -> groupHeaderClick(e));
		return table;
	}

	/**
	 * Create table by using API data which loaded from XML/JSON file
	 * 
	 * @return JTable
	 */
	public static JTable createTable(Object apiArgumentData) {
		JTable table = createTemplateTable();
		LinkedHashMap<String, List<?>> tableMap = Helpers.convertArgumentData(apiArgumentData);
		List<?> dataset = tableMap.get("Dataset");
		GroupableColumnModel model = createTableHeaders(tableMap);
		DefaultTableModel tm;
		table.setColumnModel(model);
		tm = new DefaultTableModel(0, model.getColumnCount());
		for (int rowIndex = 0; rowIndex < dataset.size(); rowIndex++) {
			List<?> rowData = (List<?>) dataset.get(rowIndex);
			Object[] row = rowData.toArray(new Object[rowData.size()]);
			tm.addRow(row);
		}
		table.setModel(tm);
		table.setTableHeader(new GroupableTableHeader(table.getColumnModel()));
		JTableHeader tblHeader = table.getTableHeader();
		// Extend group header mouse event
		tblHeader.addMouseListener((ClickedListener) (e) -> groupHeaderClick(e));
		return table;
	}

	/**
	 * Create column
	 * 
	 * @param title      String
	 * @param modelIndex int
	 * @return TableColumn
	 */
	public static TableColumn createColumn(String title, int modelIndex) {
		TableColumn column = new TableColumn();
		column.setHeaderValue(title);
		column.setIdentifier(title);
		column.setModelIndex(modelIndex);
		if (Constants.COMBO_BOX_CELL_BINDINGS.containsKey(title)) {
			Object[] values = Helpers.listToArray(Constants.COMBO_BOX_CELL_BINDINGS.get(title));
			column.setCellEditor(new CellComboBoxEditor(values));
			column.setCellRenderer(new CellComboBoxRenderer(values));
		}
		if(title.equals("Condition")) {
			column.setCellEditor(new CellConditionEditor());
		}
		return column;
	}

	/**
	 * Create table headers by using the default template which defined on
	 * configuration folder
	 * 
	 * @return GroupableColumnModel
	 */
	public static GroupableColumnModel createTableHeaders() {
		int colCount = 0;
		GroupableColumnModel model = new GroupableColumnModel();
		// Object data parse from XML/JSON file
		LinkedHashMap<?, ?> headerData = Helpers.loadDataFromFile(Constants.DEFAULT_TABLE_HEADER);
		// Use for-each loop for iteration over entrySet()
		for (Entry<?, ?> entry : headerData.entrySet()) {
			// Get group title from map entry and add (+) to the group
			IColumnGroup headerGroup = model.addGroup((String) entry.getKey() + " (+)");
			// Get map entry's value before proceed to work
			Object entryValue = entry.getValue();
			// Handle groups defined their names
			if (entryValue instanceof ArrayList) {
				for (Object el : ((ArrayList<?>) entryValue)) {
					TableColumn column = createColumn((String) el, colCount);
					model.addColumn(column);
					headerGroup.addColumn(column);
					colCount++;
				}
			}
			// Handle groups that only defines their number of children columns
			if (entryValue instanceof Long) {
				for (int i = 1; i < (Long) entryValue; i++) {
					TableColumn column = createColumn(Integer.toString(i), colCount);
					model.addColumn(column);
					headerGroup.addColumn(column);
					colCount++;
				}
			}
		}
		return model;
	}

	/**
	 * Create table headers by using Table Map which converted from API Data
	 * 
	 * @param tableMap LinkedHashMap
	 * @return GroupableColumnModel
	 */
	public static GroupableColumnModel createTableHeaders(LinkedHashMap<String, List<?>> tableMap) {
		int colCount = 0;
		GroupableColumnModel model = new GroupableColumnModel();
		List<String> groups = Arrays.asList("Arguments", "Data");
		for (String group : groups) {
			IColumnGroup headerGroup = model.addGroup(group + " (+)");
			List<?> columnNames = tableMap.get(group);
			for (Object colName : columnNames) {
				TableColumn column = createColumn((String) colName, colCount);
				model.addColumn(column);
				headerGroup.addColumn(column);
				colCount++;
			}
		}
		return model;
	}

	@SuppressWarnings("unchecked")
	public static List<LinkedHashMap<String, Object>> convertTableToAPIData(JTable table) {
		List<LinkedHashMap<String, Object>> argumentList = new ArrayList<>();
		GroupableColumnModel gm = (GroupableColumnModel) table.getColumnModel();
		Vector<?> data = ((DefaultTableModel) table.getModel()).getDataVector();
		List<IColumnGroup> gList = gm.getGroups();
		for (int i = 0; i < data.size(); i++) {
			Vector<Object> row = (Vector<Object>) data.elementAt(i);
			LinkedHashMap<String, Object> argumentMap = new LinkedHashMap<>();
			List<LinkedHashMap<String, Object>> dataList = new ArrayList<>();
			for (IColumnGroup group : gList) {
				String groupTitle = group.getTitle();
				List<TableColumn> colList = gm.getGroupColumns(group);
				if (groupTitle.contains("Argument")) {
					for (TableColumn col : colList) {
						Object colTitle = col.getHeaderValue();
						argumentMap.put(Helpers.fromTitleToAttribute((String) colTitle),
								row.elementAt(gm.getColumnIndex(colTitle)));
					}
				}
				if (groupTitle.contains("Data")) {
					for (TableColumn col : colList) {
						Object colTitle = col.getHeaderValue();
						Object cellValue = row.elementAt(gm.getColumnIndex(colTitle));
						if(cellValue != null) {
							LinkedHashMap<String, Object> dataMap = new LinkedHashMap<>();
							dataMap.put("-idx", colTitle);
							dataMap.put("#text", row.elementAt(gm.getColumnIndex(colTitle)));
							dataList.add(dataMap);
						}
					}
				}
			}
			if(dataList.size()==1) {
				argumentMap.put("Data", dataList.get(0));
			} else if (dataList.size() > 1) {
				argumentMap.put("Data", dataList);
			}
			argumentList.add(argumentMap);
		}
		return argumentList;
	}

	/**
	 * 
	 * @param ev
	 */
	// Click event
	private static void groupHeaderClick(MouseEvent ev) {
		if (ev.getComponent() instanceof GroupableTableHeader) {
			GroupableTableHeader header = (GroupableTableHeader) ev.getComponent();
			// Only handle when header is enable or Mouse Left Click
			if (!header.isEnabled() || ev.getButton() == MouseEvent.BUTTON3) {
				return;
			}
			JTable tbl = header.getTable();
			DefaultTableModel tm = (DefaultTableModel) tbl.getModel();
			GroupableColumnModel cm = (GroupableColumnModel) tbl.getColumnModel();
			header.setDraggedColumn(null);
			header.setResizingColumn(null);
			header.setDraggedDistance(0);
			Point p = ev.getPoint();
			// First find which header cell was hit
			TableColumnModel columnModel = header.getColumnModel();
			GroupableColumnModel groupModel = null;
			if (columnModel instanceof GroupableColumnModel) {
				groupModel = (GroupableColumnModel) columnModel;
			}
			int index = header.columnAtPoint(p);
			if (index != -1) {
				boolean clickedGroupHeader = false;
				if (groupModel != null) {
					TableColumn column = columnModel.getColumn(index);
					Rectangle groupBounds = header.getBoundsFor(column);
					if (groupBounds.contains(p)) {
						clickedGroupHeader = true;
					}
				}
				if (clickedGroupHeader) {
					int columnIndex = header.columnAtPoint(p);
					TableColumn column = columnModel.getColumn(columnIndex);
					IColumnGroup columnGroup = groupModel.getColumnGroupFor(column);
					String groupHeaderTitle = columnGroup.getTitle();
					System.out.println("Mouse Event on " + columnGroup.getTitle());
					if (groupHeaderTitle.contains("Arguments")) {
						tm.addRow(new Object[cm.getColumnCount()]);
						tbl.setModel(tm);
					} else {
						try {
							Integer dataColIndex = columnGroup.getColumnCount() + 1;
							Integer colIndex = tbl.getColumnCount();
							TableColumn col = JTableHelpers.createColumn(dataColIndex.toString(), colIndex);
							// Add column to headers
							cm.addColumn(col);
							// Group column to Data group
							columnGroup.addColumn(cm.getColumn(cm.getColumnIndex(dataColIndex.toString())));
							// Add column value to table model
							tm.addColumn(null);
							tbl.setModel(tm);
						} catch (Exception ex) {
							ex.printStackTrace();
						}
					}
				} else {
					if (ev.getButton() == MouseEvent.BUTTON1) {
						// The last 3 pixels + 3 pixels of next column are for resizing
						TableColumn resizingColumn = header.getResizingColumn(p, index);
						if (header.canResizeColumn(ev.getPoint(), resizingColumn)) {
							header.setResizingColumn(resizingColumn);
						} else if (header.getReorderingAllowed()) {
							TableColumn hitColumn = columnModel.getColumn(index);
							header.setDraggedColumn(hitColumn);
						}
					}
				}
			}
		}
	};
}
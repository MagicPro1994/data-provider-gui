// Generated from Condition.g4 by ANTLR 4.7.2
package antlr4.exparser;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link ConditionParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface ConditionVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link ConditionParser#parse_condition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParse_condition(ConditionParser.Parse_conditionContext ctx);
	/**
	 * Visit a parse tree produced by {@link ConditionParser#method_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMethod_expression(ConditionParser.Method_expressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link ConditionParser#methodCall}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMethodCall(ConditionParser.MethodCallContext ctx);
	/**
	 * Visit a parse tree produced by {@link ConditionParser#methodCallArguments}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMethodCallArguments(ConditionParser.MethodCallArgumentsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code binaryExpression}
	 * labeled alternative in {@link ConditionParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBinaryExpression(ConditionParser.BinaryExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code decimalExpression}
	 * labeled alternative in {@link ConditionParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDecimalExpression(ConditionParser.DecimalExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code stringExpression}
	 * labeled alternative in {@link ConditionParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStringExpression(ConditionParser.StringExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code boolExpression}
	 * labeled alternative in {@link ConditionParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolExpression(ConditionParser.BoolExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code identifierExpression}
	 * labeled alternative in {@link ConditionParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdentifierExpression(ConditionParser.IdentifierExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code notExpression}
	 * labeled alternative in {@link ConditionParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNotExpression(ConditionParser.NotExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code specialExpression}
	 * labeled alternative in {@link ConditionParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSpecialExpression(ConditionParser.SpecialExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code parenExpression}
	 * labeled alternative in {@link ConditionParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParenExpression(ConditionParser.ParenExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code comparatorExpression}
	 * labeled alternative in {@link ConditionParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComparatorExpression(ConditionParser.ComparatorExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link ConditionParser#methodName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMethodName(ConditionParser.MethodNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link ConditionParser#specialName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSpecialName(ConditionParser.SpecialNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link ConditionParser#ambiguousName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAmbiguousName(ConditionParser.AmbiguousNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link ConditionParser#comparator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComparator(ConditionParser.ComparatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link ConditionParser#binary}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBinary(ConditionParser.BinaryContext ctx);
	/**
	 * Visit a parse tree produced by {@link ConditionParser#bool}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBool(ConditionParser.BoolContext ctx);
}
package dataprovider.enhancements;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.github.underscore.lodash.U;

public class Helpers {
	public static String loadTextFromFile(String filePath) {
		File file = new File(filePath);
		String result = "";
		FileReader fr;
		try {
			fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String currLine = null;
			while ((currLine = br.readLine()) != null) {
				result += currLine;
			}
			fr.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}

	public static void writeTextToFile(String filePath, String textContent) {
		try {
			File newTextFile = new File(filePath);

			FileWriter fw = new FileWriter(newTextFile);
			fw.write(textContent);
			fw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static LinkedHashMap<?, ?> loadDataFromFile(String filePath) {
		try {
			if (filePath.endsWith(".json")) {
				return fromJson(loadTextFromFile(filePath));
			}
			if (filePath.endsWith(".xml")) {
				return fromXml(loadTextFromFile(filePath));
			}
		} catch (Exception e) {
			System.out.println("Cannot load file");
		}
		System.out.println("File path doesn't exist or is not supported");
		return null;
	}

	public static LinkedHashMap<?, ?> fromJson(String textContent) {
		try {
			return (LinkedHashMap<?, ?>) U.fromJson(textContent);
		} catch (Exception e) {
			System.out.println("Cannot parse JSON from text");
		}
		return null;
	}

	public static LinkedHashMap<?, ?> fromXml(String textContent) {
		try {
			return (LinkedHashMap<?, ?>) U.fromXml(textContent);
		} catch (Exception e) {
			System.out.println("Cannot parse XML from text");
		}
		return null;
	}

	public static String fromAttributeToTitle(String key) {
		Pattern p = Pattern.compile("^-([a-z])(.*)");
		Matcher m = p.matcher(key);
		if (m.find()) {
			return (m.group(1).toUpperCase() + m.group(2));
		}
		System.out.println("This key is not an attribute");
		return key;
	}

	public static String fromTitleToAttribute(String key) {
		Pattern p = Pattern.compile("^([A-Z])(.*)");
		key = key.replace(" ", "-");
		Matcher m = p.matcher(key);
		if (m.find()) {
			return ("-" + m.group(1).toLowerCase() + m.group(2));
		}
		System.out.println("This key is not a title");
		return key;
	}

	@SuppressWarnings("unchecked")
	public static void processAPIArgumentData(LinkedHashMap<String, List<?>> tableMap, Object apiArgumentData) {
		List<? super String> argumentGroup = (List<? super String>) tableMap.getOrDefault("Arguments",
				new ArrayList<>());
		List<? super String> dataGroup = (List<? super String>) tableMap.getOrDefault("Data", new ArrayList<>());
		List<? super List<String>> dataset = (List<? super List<String>>) tableMap.getOrDefault("Dataset",
				new ArrayList<>());
		List<String> dataRow = new ArrayList<>();
		for (Entry<?, ?> entry : ((LinkedHashMap<?, ?>) apiArgumentData).entrySet()) {
			Object entryKey = entry.getKey();
			Object entryValue = entry.getValue();
			if (entryValue instanceof String) {
				String key = Helpers.fromAttributeToTitle((String) entryKey);
				if (!argumentGroup.contains(key)) {
					argumentGroup.add(Helpers.fromAttributeToTitle((String) entryKey));
				}
				dataRow.add((String) entryValue);
			}
			if (entryKey.equals("Data")) {
				LinkedHashMap<String, List<String>> tmp = processDataGroup(entryValue);
				// Remove all duplicates before adding
				dataGroup.removeAll(tmp.get("Columns"));
				dataGroup.addAll(tmp.get("Columns"));
				dataRow.addAll(tmp.get("Values"));
			}
		}
		dataset.add(dataRow);
		tableMap.put("Arguments", argumentGroup);
		tableMap.put("Data", dataGroup);
		tableMap.put("Dataset", dataset);
	}

	public static LinkedHashMap<String, List<String>> processDataGroup(Object argumentData) {
		LinkedHashMap<String, List<String>> dataMap = new LinkedHashMap<>();
		List<String> columns = new ArrayList<>();
		List<String> dataset = new ArrayList<>();
		if (argumentData instanceof LinkedHashMap) {
			columns.add((String) ((LinkedHashMap<?, ?>) argumentData).get("-idx"));
			dataset.add((String) ((LinkedHashMap<?, ?>) argumentData).get("#text"));
		}
		if (argumentData instanceof ArrayList) {
			for (Object el : ((ArrayList<?>) argumentData)) {
				if (el instanceof LinkedHashMap) {
					columns.add((String) ((LinkedHashMap<?, ?>) el).get("-idx"));
					dataset.add((String) ((LinkedHashMap<?, ?>) el).get("#text"));
				}
			}
		}
		dataMap.put("Columns", columns);
		dataMap.put("Values", dataset);
		return dataMap;
	}

	public static LinkedHashMap<String, List<?>> convertArgumentData(Object apiArgumentData) {
		LinkedHashMap<String, List<?>> tableMap = new LinkedHashMap<>();
		if (apiArgumentData instanceof LinkedHashMap) {
			processAPIArgumentData(tableMap, apiArgumentData);
			return tableMap;
		}

		if (apiArgumentData instanceof ArrayList) {
			for (Object el : ((ArrayList<?>) apiArgumentData)) {
				processAPIArgumentData(tableMap, el);
			}
		}
		return tableMap;
	}

	@SuppressWarnings("unchecked")
	public static <T> T[] listToArray(List<T> lst) {
		return lst.toArray((T[]) new Object[lst.size()]);
	}
}
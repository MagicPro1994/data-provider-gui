Short-cut keys:
- Control + L: Load application data from XML file
- Control + E: Export application data to XML file
- Control + N: New case panel
- F2: Rename current case panel
- Control + D: Delete current case panel
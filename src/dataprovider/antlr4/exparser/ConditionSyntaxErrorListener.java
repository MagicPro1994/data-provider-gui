package dataprovider.antlr4.exparser;

import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import org.antlr.v4.runtime.misc.Utils;

import java.util.ArrayList;
import java.util.List;

public class ConditionSyntaxErrorListener extends BaseErrorListener {
	private final List<ConditionSyntaxError> syntaxErrors = new ArrayList<>();

	ConditionSyntaxErrorListener() {
	}

	List<ConditionSyntaxError> getSyntaxErrors() {
		return syntaxErrors;
	}

	@Override
	public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine,
			String msg, RecognitionException e) {
		syntaxErrors.add(new ConditionSyntaxError(recognizer, offendingSymbol, line, charPositionInLine, msg, e));
	}

	@Override
	public String toString() {
		return Utils.join(syntaxErrors.iterator(), "\n");
	}
}
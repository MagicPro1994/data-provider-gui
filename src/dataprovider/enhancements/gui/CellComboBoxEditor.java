package dataprovider.enhancements.gui;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;

public class CellComboBoxEditor extends DefaultCellEditor {
	private static final long serialVersionUID = -8211621513077087931L;

	public CellComboBoxEditor(Object[] items) {
		super(new JComboBox<Object>(items));
	}

}

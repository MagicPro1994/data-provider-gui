package test.parse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;

import com.github.underscore.lodash.U;

import dataprovider.enhancements.Helpers;

public class TestArgumentParse {
	private static void processAPIArgumentData(LinkedHashMap<String, List<String>> tableMap, Object apiArgumentData) {
		List<String> argumentGroup = tableMap.getOrDefault("Arguments", new ArrayList<>());
		List<String> dataGroup = tableMap.getOrDefault("Data", new ArrayList<>());
		List<String> dataset = tableMap.getOrDefault("Dataset", new ArrayList<>());
		for (Entry<?, ?> entry : ((LinkedHashMap<?, ?>) apiArgumentData).entrySet()) {
			Object entryKey = entry.getKey();
			Object entryValue = entry.getValue();
			if (entryValue instanceof String) {
				String key = Helpers.fromAttributeToTitle((String) entryKey);
				if(!argumentGroup.contains(key)) {
					argumentGroup.add(Helpers.fromAttributeToTitle((String) entryKey));
				}
				dataset.add((String) entryValue);
			}
			if (entryKey.equals("Data")) {
				LinkedHashMap<String, List<String>> tmp = processDataGroup(entryValue);
				dataGroup.removeAll(tmp.get("Columns"));
				dataGroup.addAll(tmp.get("Columns"));
				dataset.addAll(tmp.get("Values"));
			}
		}
		tableMap.put("Arguments", argumentGroup);
		tableMap.put("Data", dataGroup);
		tableMap.put("Dataset", dataset);

	}

	private static LinkedHashMap<String, List<String>> processDataGroup(Object argumentData) {
		LinkedHashMap<String, List<String>> dataMap = new LinkedHashMap<>();
		List<String> columns = new ArrayList<>();
		List<String> dataset = new ArrayList<>();
		if (argumentData instanceof LinkedHashMap) {
			columns.add((String) ((LinkedHashMap<?, ?>) argumentData).get("-idx"));
			dataset.add((String) ((LinkedHashMap<?, ?>) argumentData).get("#text"));
		}
		if (argumentData instanceof ArrayList) {
			for (Object el : ((ArrayList<?>) argumentData)) {
				if (el instanceof LinkedHashMap) {
					columns.add((String) ((LinkedHashMap<?, ?>) el).get("-idx"));
					dataset.add((String) ((LinkedHashMap<?, ?>) el).get("#text"));
				}
			}
		}
		dataMap.put("Columns", columns);
		dataMap.put("Values", dataset);
		return dataMap;
	}

	private static LinkedHashMap<?, ?> convertArgumentData(Object apiArgumentData) {
		LinkedHashMap<String, List<String>> tableMap = new LinkedHashMap<>();

		if (apiArgumentData instanceof LinkedHashMap) {
			processAPIArgumentData(tableMap, apiArgumentData);
			System.out.println(tableMap);
			return tableMap;
		}

		if (apiArgumentData instanceof ArrayList) {
			for (Object el : ((ArrayList<?>) apiArgumentData)) {
				processAPIArgumentData(tableMap, el);
			}
		}
		System.out.println(tableMap);
		return tableMap;
	}

	public static void main(String[] args) {
		String filepath = System.getProperty("user.dir") + "\\test\\test-data-0.xml";
		System.out.println(U.xmlToJson(Helpers.loadTextFromFile(filepath)));
		System.out.println(Helpers.loadDataFromFile(filepath));
		//convertArgumentData(Helpers.loadDataFromFile(filepath).get("Argument"));
		List<Object> a = Arrays.asList("A", "B", "C");
		Object[] array = a.toArray(new Object[a.size()]);
		System.out.println(array[0]);
	}

}

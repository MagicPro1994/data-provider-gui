package dataprovider.common;

import java.util.LinkedHashMap;
import java.util.List;

import dataprovider.enhancements.Helpers;

@SuppressWarnings("unchecked")
public class Constants {
	public final static String CURRENT_DIR = System.getProperty("user.dir");
	public final static String SETTING_FILE = CURRENT_DIR + "\\config\\settings.json";
	public final static String DEFAULT_TABLE_HEADER = CURRENT_DIR + "\\config\\default-table-header.json";
	public final static String DEFAULT_TEMPLATE_FILE = CURRENT_DIR + "\\config\\default-data.json";
	public final static LinkedHashMap<String, List<String>> COMBO_BOX_CELL_BINDINGS = (LinkedHashMap<String, List<String>>) Helpers
			.loadDataFromFile(CURRENT_DIR + "\\config\\combo-box-cell-bindings.json");
}